﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using ourMove.Models.Positions;
using YourMove.Models.General;
using YourMove.Models.Moves;
using YourMove.Models.Pieces;
using YourMove.Models.Positions;

namespace YourMove.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChessMovesController : ControllerBase
    {
        // TODO: Remove this duplication that exists in console app
        private readonly Dictionary<string, KeyValuePair<string, Type>> pieces = new List<Type>()
            { typeof(Rook), typeof(Bishop), typeof(King), typeof(Queen) }
            .ToDictionary(t => t.Name.Substring(0, 1).ToUpper(), t => new KeyValuePair<string, Type>(t.Name, t));

        private bool ChessPieceSelection(string selection, out string message)
        {
            message = string.Empty;

            var selectionToUse = selection.ToUpper().Trim();

            if (!pieces.ContainsKey(selectionToUse))
            {
                message = $"You selection of {selection} is not valid, options are {string.Join(", ", pieces.Keys)}";
                return false;
            }
            return true;
        }

        [HttpGet("")]
        public IActionResult GetPieceOptions()
        {
            return new JsonResult(new PieceOptionsDto(
                pieces
                    .Select(kv => new CodeWithNameDto { Code = kv.Key, Name = kv.Value.Key }).ToList(),

                new PositionOptionsDto(
                        PiecePosition.ValidRows(),
                        PiecePosition.ValidColumns()
                    )
                )
            );
        }

        [HttpPost]
        public IActionResult SetPieceAtPosition([FromForm] string piece, [FromForm] string position)
        {

            if (!ChessPieceSelection(piece ?? string.Empty, out string errMessage))
            {
                return new JsonResult(new { Message = errMessage }) { StatusCode = (int?)HttpStatusCode.BadRequest };
            }

            PiecePosition positionToUse;

            try
            {
                positionToUse = new PiecePosition(position ?? string.Empty);
            }
            catch (ArgumentException ex)
            {
                return new JsonResult(new { Message = ex.Message }) { StatusCode = (int?)HttpStatusCode.BadRequest };
            }

            KeyValuePair<string, Type> pieceSelection = pieces[piece.ToUpper()];

            ChessPiece pieceCreated = (ChessPiece)Activator.CreateInstance(pieceSelection.Value, positionToUse);

            return new JsonResult(new  PieceMovesOptions(pieceCreated));
        }
    }
}
