﻿using System;
using System.Collections.Generic;
using System.Linq;
using YourMove.Models.Pieces;
using YourMove.Models.Positions;

namespace CommandLineChessPiecePositions
{
    class Program
    {
        // TODO: break into method and use loops with exit condition to
        //      allow user to make correct inputs with re-running app
        static void Main(string[] args)
        {
            var pieces = new List<Type>() { typeof(Rook), typeof(Bishop), typeof(King), typeof(Queen) }
                .ToDictionary(t => t.Name.Substring(0, 1).ToUpper(), t => new KeyValuePair<string, Type>(t.Name, t));

            var piecePrompts = pieces.Select(kv => $"'{kv.Key}' for {kv.Value.Key}")
                .ToList();

            Console.WriteLine("Enter a chess piece:");
            Console.WriteLine($"{string.Join(Environment.NewLine, piecePrompts)}");

            string pieceSelection = Console.ReadLine().ToUpper();

            if(!pieces.ContainsKey(pieceSelection))
            {
                Console.WriteLine(
                    $"You selection of {pieceSelection} is not valid, options are {string.Join(", ", pieces.Keys)}");
                return;
            }

            Console.WriteLine( $"You have chosen {pieces[pieceSelection].Key} as your piece");

            Console.WriteLine("Choose a place on the board eg: 'B4'");

            string positionSelection = Console.ReadLine();

            PiecePosition position;

            try
            {
                position = new PiecePosition(positionSelection);
            }
            catch(ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            ChessPiece piece = (ChessPiece)Activator.CreateInstance(pieces[pieceSelection].Value, position);

            Console.WriteLine("All possible moves are " + string.Join(", ", piece.AllPossiblePositions()));
        }
    }
}
