﻿using System;
namespace YourMove.Models.General
{
    public class CodeWithNameDto
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
