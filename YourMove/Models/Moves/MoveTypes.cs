﻿namespace YourMove.Models.Moves
{
    public enum MoveTypes
    {
        Up = 1,
        Right,
        Down,
        Left,
        Diagonal1_30,
        Diagonal4_30,
        Diagonal7_30,
        Diagonal10_30
    }
}
