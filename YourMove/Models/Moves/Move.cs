﻿using System;
using System.Collections.Generic;
using System.Linq;
using YourMove.Models.Positions;

namespace YourMove.Models.Moves
{
    public abstract class Move
    {
        public virtual bool CanHandleMove(MoveTypes moveType)
        {
            return ApplicableMoves().Contains(moveType);
        }

        public abstract IList<MoveTypes> ApplicableMoves();

        public virtual IEnumerable<PiecePosition> PositionsCanStep(PiecePosition currentPosition, MoveTypes moveType, uint? numberOfSteps = null)
        {
            var positionsToReturn = new List<PiecePosition>();
            
            int numberOfStepsToUse = (int)numberOfSteps.GetValueOrDefault(8);
            int currentStep = 1;
            PiecePosition lastPosition = null;

            while (currentStep <= numberOfStepsToUse)
            {
                var positonTouse = lastPosition ?? currentPosition;
                lastPosition = OneMove(positonTouse, moveType);
                if (lastPosition == null || positonTouse == lastPosition)
                {
                    break;
                }
                positionsToReturn.Add(lastPosition);
                currentStep++;
            }

            return positionsToReturn;
        }

        public virtual PiecePosition OneMove(PiecePosition currentPosition, MoveTypes moveType)
        {
            if (!CanHandleMove(moveType))
            {
                return null;
            }
            var nextPos = NextPosition(currentPosition, moveType, 1);
            return nextPos == currentPosition ? null : nextPos;
        }

        protected abstract PiecePosition NextPosition(PiecePosition currentPosition, MoveTypes moveType, int numberOfSteps);

        protected virtual PiecePosition MakePosition(int row, int col, bool failIfOutOfRange = false)
        {
            var rowMembers = PiecePosition.ValidRows();

            var colMembers = PiecePosition.ValidColumns();

            var maxRow = rowMembers.Max();
            var minRow = rowMembers.Min();

            var maxCol = colMembers.Max();
            var minCol = colMembers.Min();

            if (row < minRow)
            {
                throwBecauseOutOfRange(failIfOutOfRange);
                row = minRow;
            }

            if (row > maxRow)
            {
                throwBecauseOutOfRange(failIfOutOfRange);
                row = maxRow;
            }

            if (col < minCol)
            {
                throwBecauseOutOfRange(failIfOutOfRange);
                col = minCol;
            }

            if (col > maxCol)
            {
                throwBecauseOutOfRange(failIfOutOfRange);
                col = maxCol;
            }

            return new PiecePosition((RowPositions)row, (ColumnPositions)col);
        }

        private static void throwBecauseOutOfRange(bool failIfOutOfRange)
        {
            if (failIfOutOfRange)
            {
                throw new ArgumentOutOfRangeException("Value is out range");
            }
        }
    }
}
