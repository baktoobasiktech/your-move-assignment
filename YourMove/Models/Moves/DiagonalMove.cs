﻿using System;
using System.Collections.Generic;
using System.Linq;
using YourMove.Models.Positions;

namespace YourMove.Models.Moves
{
    public class DiagonalMove : Move
    {
        public DiagonalMove()
        {
            _applicableMoves = new List<MoveTypes> {
                MoveTypes.Diagonal1_30, MoveTypes.Diagonal4_30,
                MoveTypes.Diagonal7_30, MoveTypes.Diagonal10_30
            };
        }

        private readonly List<MoveTypes> _applicableMoves;

        public override IList<MoveTypes> ApplicableMoves()
        {
            return _applicableMoves.AsReadOnly();
        }

        protected override PiecePosition NextPosition(PiecePosition currentPosition, MoveTypes moveType, int numberOfSteps)
        {
            char row = (char)currentPosition.Row;
            int col = (int)currentPosition.Column;

            switch (moveType)
            {
                case MoveTypes.Diagonal1_30:
                    col += numberOfSteps;
                    row += (char)numberOfSteps;
                    break;
                case MoveTypes.Diagonal4_30:
                    col -= numberOfSteps;
                    row += (char)numberOfSteps;
                    break;
                case MoveTypes.Diagonal7_30:
                    col -= numberOfSteps;
                    row -= (char)numberOfSteps;
                    break;
                case MoveTypes.Diagonal10_30:
                    col += numberOfSteps;
                    row -= (char)numberOfSteps;
                    break;
            }

            try
            {
                return MakePosition(row, col, true);
            }
            catch (ArgumentOutOfRangeException)
            {
                return new PiecePosition(currentPosition.Row, currentPosition.Column);
            }

        }
    }
}
