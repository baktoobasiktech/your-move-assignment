﻿using System;
using System.Collections.Generic;
using System.Linq;
using YourMove.Models.Pieces;

namespace YourMove.Models.Moves
{
    public class PieceMovesOptions
    {
        public string Piece { get; }
        public string Position { get; }
        public IEnumerable<string> Moves { get; }

        public PieceMovesOptions(ChessPiece piece)
        {
            Piece = piece.GetType().Name;
            Position = piece.Position.ToString();
            Moves = piece.AllPossiblePositions().Select(p => p.ToString()).ToList();
        }
    }
}
