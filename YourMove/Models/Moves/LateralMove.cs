﻿using System;
using System.Collections.Generic;
using System.Linq;
using YourMove.Models.Positions;

namespace YourMove.Models.Moves
{
    public class LateralMove : Move
    {
        public LateralMove()
        {
            _applicableMoves = new List<MoveTypes> {
                MoveTypes.Up, MoveTypes.Right,
                MoveTypes.Down, MoveTypes.Left
            };
        }

        private readonly List<MoveTypes> _applicableMoves;

        public override IList<MoveTypes> ApplicableMoves()
        {
            return _applicableMoves.AsReadOnly();
        }

        protected override PiecePosition NextPosition(PiecePosition currentPosition, MoveTypes moveType, int numberOfSteps)
        {
            char row = (char)currentPosition.Row;
            int col = (int)currentPosition.Column;

            switch (moveType)
            {
                case MoveTypes.Up:
                    col += numberOfSteps;
                    break;
                case MoveTypes.Right:
                    row += (char)numberOfSteps;
                    break;
                case MoveTypes.Down:
                    col -= numberOfSteps;
                    break;
                case MoveTypes.Left:
                    row -= (char)numberOfSteps;
                    break;
            }

            return MakePosition(row, col);
        }
    }
}
