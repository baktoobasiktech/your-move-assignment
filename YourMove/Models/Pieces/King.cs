﻿using System;
using System.Collections.Generic;
using System.Linq;
using YourMove.Models.Moves;
using YourMove.Models.Positions;

namespace YourMove.Models.Pieces
{
    public class King : ChessPiece
    {
        public King(PiecePosition position)
        {
            moves = new List<Move> { new LateralMove(), new DiagonalMove() };
            Position = position;
            MovesPerTurn = 1;
        }

        public override PiecePosition Position { get; }
        public override uint MovesPerTurn { get; protected set; }
    }
}
