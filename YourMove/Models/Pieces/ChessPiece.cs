﻿using System;
using System.Collections.Generic;
using System.Linq;
using YourMove.Models.Moves;
using YourMove.Models.Positions;

namespace YourMove.Models.Pieces
{
    public abstract class ChessPiece
    {
        protected IEnumerable<Move> moves = Enumerable.Empty<Move>();

        public abstract PiecePosition Position { get; }

        public abstract uint MovesPerTurn { get; protected set; }

        public IEnumerable<PiecePosition> AllPossiblePositions()
        {
            return moves
                .SelectMany(m =>
                {
                    var applicableMoves = m.ApplicableMoves();
                    return applicableMoves
                    .SelectMany(a => m.PositionsCanStep(Position, a, MovesPerTurn));
                })
                .ToList();
        }

        public IEnumerable<PiecePosition> PossiblePositionsForMove(MoveTypes move)
        {
            return moves
                .Where(m => m.CanHandleMove(move))
                .SelectMany(m =>
                {
                    var applicableMoves = m.ApplicableMoves();
                    return applicableMoves.SelectMany(a => m.PositionsCanStep(Position, a, MovesPerTurn));
                })
                .ToList();
        }
    }
}
