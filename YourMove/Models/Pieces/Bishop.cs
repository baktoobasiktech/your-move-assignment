﻿using System;
using System.Collections.Generic;
using YourMove.Models.Moves;
using YourMove.Models.Positions;

namespace YourMove.Models.Pieces
{
    public class Bishop : ChessPiece
    {
        public Bishop(PiecePosition position)
        {
            moves = new List<Move> { new DiagonalMove() };
            Position = position;
            MovesPerTurn = 8;
        }

        public override PiecePosition Position { get; }
        public override uint MovesPerTurn { get; protected set; }
    }
}
