﻿using System;
using System.Collections.Generic;
using ourMove.Models.Positions;
using YourMove.Models.General;

namespace YourMove.Models.Positions
{
    public class PieceOptionsDto
    {
        public PieceOptionsDto(IEnumerable<CodeWithNameDto> pieces, PositionOptionsDto positionsOptions)
        {
            Pieces = pieces ?? throw new ArgumentNullException(nameof(pieces));
            PositionsOptions = positionsOptions ?? throw new ArgumentNullException(nameof(positionsOptions));
        }

        public IEnumerable<CodeWithNameDto> Pieces { get; set; }
        public PositionOptionsDto PositionsOptions { get; set; }
    }
}
