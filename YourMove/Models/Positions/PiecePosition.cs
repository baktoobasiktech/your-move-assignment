﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YourMove.Models.Positions
{
    public sealed class PiecePosition
    {
        private static List<char> _validRows;
        private static List<int> _validColumns;

        public static IEnumerable<char>ValidRows()
        {
            if(_validRows == null)
            {
                _validRows = Enum.GetValues(typeof(RowPositions))
                .Cast<RowPositions>()
                .Select(r => (char)r).ToList();
            }

            return _validRows.AsReadOnly();
        }

        public static IEnumerable<int> ValidColumns()
        {
            if (_validColumns == null)
            {
                _validColumns = Enum.GetValues(typeof(ColumnPositions))
                .Cast<ColumnPositions>()
                .Select(c => (int)c).ToList();
            }
            return _validColumns.AsReadOnly();   
        }

        public static bool IsValidRow(string row)
        {
            return row.Length == 1 && char.IsLetter(row[0])
                && Enum.TryParse<RowPositions>(row.ToUpper(), out _);
        }

        public static bool IsValidColumn(string col)
        {
            return col.Length == 1 && char.IsDigit(col[0])
                && ValidColumns().Contains(int.Parse(col));
        }

        public static string CheckAndFormatValidPosition(string position)
        {
            return position != null && position.Length == 2
                && IsValidRow(position[0].ToString())
                && IsValidColumn(position[1].ToString()) ? position.ToUpper() : null;
        }

        public PiecePosition(string postion)
        {
            var positionToUse = CheckAndFormatValidPosition(postion);
            var validRows = ValidRows();
            var validColumns = ValidColumns();

            if (positionToUse == null)
            {
                var errMessaeBuilder =
                    new StringBuilder("Postion is not valid must be in format [ROW][COLUMN]. eg A2." + Environment.NewLine);
                errMessaeBuilder.Append($"Valid [ROW] values are: {string.Join(", ", validRows)}{Environment.NewLine}");
                errMessaeBuilder.Append($"Valid [COLUMN] values are: {string.Join(", ", validColumns)}");

                throw new ArgumentException(errMessaeBuilder.ToString());
            }

            Row = Enum.Parse<RowPositions>(positionToUse.Substring(0, 1));
            Column = Enum.Parse<ColumnPositions>(positionToUse.Substring(1, 1));
        }

        public PiecePosition(RowPositions row, ColumnPositions column)
        {
            Row = row;
            Column = column;
        }

        public RowPositions Row { get; }
        public ColumnPositions Column { get; }

        public override bool Equals(object obj)
        {
            var point = obj as PiecePosition;
            if(point == null)
            {
                return false;
            }
            return Row == point.Row && Column == point.Column;
        }

        public override int GetHashCode()
        {
            return Row.GetHashCode() * 7 + Column.GetHashCode() * 13;
        }

        public static bool operator ==(PiecePosition pos1, PiecePosition pos2)
        {
            if (pos1 is null)
            {
                if (pos2 is null)
                {
                    return true;
                }
                return false;
            }
            return pos1.Equals(pos2);
        }

        public static bool operator !=(PiecePosition pos1, PiecePosition pos2)
        {
            return !(pos1 ==pos2);
        }

        public override string ToString()
        {
            return $"{Row}{(int)Column}";
        }
    }
}
