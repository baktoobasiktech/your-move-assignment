﻿using System;
using System.Collections.Generic;

namespace ourMove.Models.Positions
{
    public class PositionOptionsDto
    {
        public IEnumerable<char> Rows { get; }
        public IEnumerable<int> Columns { get; }

        public PositionOptionsDto(IEnumerable<char> rows, IEnumerable<int> columns)
        {
            Rows = rows;
            Columns = columns;
        }

        public override bool Equals(object obj)
        {
            return obj is PositionOptionsDto other &&
                   EqualityComparer<IEnumerable<char>>.Default.Equals(Rows, other.Rows) &&
                   EqualityComparer<IEnumerable<int>>.Default.Equals(Columns, other.Columns);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Rows, Columns);
        }
    }
}
