﻿using System;
namespace YourMove.Models.Positions
{
    public enum ColumnPositions
    {
        One = 1,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight
    }
}
