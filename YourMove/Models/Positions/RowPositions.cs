﻿namespace YourMove.Models.Positions
{
    public enum RowPositions
    {
        A = 'A',
        B = 'B',
        C = 'C',
        D = 'D',
        E = 'E',
        F = 'F',
        G = 'G',
        H = 'H'
    }
}
