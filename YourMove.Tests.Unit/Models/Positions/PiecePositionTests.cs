﻿using System;
using Xunit;
using YourMove.Models.Positions;

namespace YourMove.Tests.Unit.Models.Positions
{
    public class PiecePositionTests
    {
        [Fact]
        public void WhenConstructedWithEnumPropertiesAreCorrect()
        {
            var pos = new PiecePosition(RowPositions.C, ColumnPositions.Four);
            Assert.True(pos.Row == RowPositions.C, "Row passed in is not reflected");
            Assert.True(pos.Column == ColumnPositions.Four, "Column passed in is not reflected");
        }

        [Fact]
        public void WhenIsValidRowIsCalledWithValidUpperCaseRowStringTrueIsReturned()
        {
            Assert.True(PiecePosition.IsValidRow("A"), "Valid ROW was flagged as invalid");
        }

        [Fact]
        public void WhenIsValidRowIsCalledWithValidLowerCaseRowStringTrueIsReturned()
        {
            Assert.True(PiecePosition.IsValidRow("f"), "Valid ROW was flagged as invalid");
        }

        [Fact]
        public void WhenIsValidRowIsCalledWithInvalidUpperCaseRowStringFalseIsReturned()
        {
            Assert.False(PiecePosition.IsValidRow("O"), "Invalid ROW was flagged as valid");
        }

        [Fact]
        public void WhenIsValidRowIsCalledWithInvalidLowerCaseRowStringFalseIsReturned()
        {
            Assert.False(PiecePosition.IsValidRow("k"), "Invalid ROW was flagged as valid");
        }

        [Fact]
        public void WhenIsValidColumnIsCalledWithValidColumnStringTrueIsReturned()
        {
            Assert.True(PiecePosition.IsValidColumn("1"), "Invalid COLUMN was flagged as valid");
        }

        [Fact]
        public void WhenIsValidColumnIsCalledWithInvalidColumnStringFalseIsReturned()
        {
            Assert.False(PiecePosition.IsValidColumn("0"), "Invalid COLUMN was flagged as valid");
        }

        [Fact]
        public void WhenCheckAndFormatValidPositionIsGivenValidUpperCaseStringPostionExpectedStringIsReturned()
        {
            Assert.Equal("B5", PiecePosition.CheckAndFormatValidPosition("B5"));
        }

        [Fact]
        public void WhenCheckAndFormatValidPositionIsGivenValidLowerCaseStringPostionExpectedStringIsReturned()
        {
            Assert.Equal("C7", PiecePosition.CheckAndFormatValidPosition("c7"));
        }

        [Fact]
        public void WhenCheckAndFormatInvalidPositionIsGivenValidUpperCaseStringPostionNullIsReturned()
        {
            Assert.Null(PiecePosition.CheckAndFormatValidPosition("C11"));
        }

        [Fact]
        public void WhenCheckAndFormatInvalidPositionIsGivenValidLowerCaseStringPostionNullIsReturned()
        {
            Assert.Null(PiecePosition.CheckAndFormatValidPosition("l7"));
        }

        [Fact]
        public void WhenValidStringPositionIsGivenToStringConstructorPointIsConstructed()
        {
            _ = new PiecePosition("F4");
        }

        [Fact]
        public void WhenInvalidStringPositionIsGivenToStringConstructorPointIsConstructed()
        {
            Assert.Throws<ArgumentException>(() =>new PiecePosition("F9"));
        }

        [Fact]
        public void WhenValidStringPositionIsGivenToUpperCaseStringConstructorExpectedPositionRowIsCreated()
        {
            var pos = new PiecePosition("F4");

            Assert.Equal(RowPositions.F, pos.Row);
            Assert.Equal(ColumnPositions.Four, pos.Column);
        }

        [Fact]
        public void WhenValidStringPositionIsGivenToLowerCaseStringConstructorExpectedPositionRowIsCreated()
        {
            var pos = new PiecePosition("g6");

            Assert.Equal(RowPositions.G, pos.Row);
            Assert.Equal(ColumnPositions.Six, pos.Column);
        }

        [Fact]
        public void WhenEqualsMethodIsCalledOnSamePositiObjectonTheyAreEqual()
        {
            var pos = new PiecePosition(RowPositions.C, ColumnPositions.Six);
            Assert.Equal(pos, pos);
        }

        [Fact]
        public void WhenEqualsMethodIsCalledOnPositionsTheyHaveSamedPointsTheyAreEqual()
        {
            var pos1 = new PiecePosition(RowPositions.C, ColumnPositions.Six);
            var pos2 = new PiecePosition(RowPositions.C, ColumnPositions.Six);
            Assert.Equal(pos1, pos2);
        }

        [Fact]
        public void WhenEqualsMethodIsCalledOnPositionsTheyHaveDifferentPointsTheyAreNotEqual()
        {
            var pos1 = new PiecePosition(RowPositions.C, ColumnPositions.Six);
            var pos2 = new PiecePosition(RowPositions.G, ColumnPositions.Six);
            Assert.NotEqual(pos1, pos2);
        }
    }
}
