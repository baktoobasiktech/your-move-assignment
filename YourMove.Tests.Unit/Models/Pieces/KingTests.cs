﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using YourMove.Models.Pieces;
using YourMove.Models.Positions;

namespace YourMove.Tests.Unit.Models.Pieces
{
    public class KinTests
    {
        [Fact]
        public void AllPossiblePositionsForMoveReturnsExpectedSetOfPositions()
        {
            var king = new King(new PiecePosition("g5"));

            var allPossibleMoves = king.AllPossiblePositions();
            var expectedPositions = new List<string> {
                "h5", "f5", "g6", "g4", "f6", "f4", "h4", "h6"};

            Assert.Equal(expectedPositions.Count, allPossibleMoves.Count());

            Assert.All(expectedPositions, (string pos) => {
                Assert.Contains(allPossibleMoves, p => p == new PiecePosition(pos));
            });
        }
    }
}
