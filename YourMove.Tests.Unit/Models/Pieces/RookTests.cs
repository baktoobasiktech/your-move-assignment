﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using YourMove.Models.Moves;
using YourMove.Models.Pieces;
using YourMove.Models.Positions;

namespace YourMove.Tests.Unit.Models.Pieces
{
    public class RookTests
    {
        [Fact]
        public void AllPossiblePositionsForMoveReturnsExpectedSetOfPositions()
        {
            var rook = new Rook(new PiecePosition("g5"));

            var allPossibleMoves = rook.AllPossiblePositions();
            var expectedPositions = new List<string> {
                "g6", "g7", "g8", "g4", "g3", "g2", "g1",
                "a5", "b5","c5","d5","e5","f5","h5"};

            Assert.Equal(expectedPositions.Count, allPossibleMoves.Count());

            Assert.All(expectedPositions, (string pos) => {
                     Assert.Contains(allPossibleMoves, p => p == new PiecePosition(pos));
                });
        }

        [Fact]
        public void PossiblePositionsForMoveReturnsEmptyCollectionWhenMovementIsDiagnal()
        {
            var rook = new Rook(new PiecePosition("g5"));

            Assert.Empty(rook.PossiblePositionsForMove(MoveTypes.Diagonal10_30));
        }
    }
}
