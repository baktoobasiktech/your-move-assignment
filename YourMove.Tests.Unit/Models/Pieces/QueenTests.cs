﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using YourMove.Models.Pieces;
using YourMove.Models.Positions;

namespace YourMove.Tests.Unit.Models.Pieces
{
    public class QueenTests
    {
        [Fact]
        public void AllPossiblePositionsForMoveReturnsExpectedSetOfPositions()
        {
            var queen = new Queen(new PiecePosition("g5"));

            var allPossibleMoves = queen.AllPossiblePositions();
            var expectedPositions = new List<string> {
                "g6", "g7", "g8", "g4", "g3", "g2", "g1",
                "a5", "b5","c5","d5","e5","f5","h5",
                "h6", "f4", "e3", "d2", "c1", "h4", "f6",
                "e7", "d8"};

            Assert.Equal(expectedPositions.Count, allPossibleMoves.Count());

            Assert.All(expectedPositions, (string pos) => {
                Assert.Contains(allPossibleMoves, p => p == new PiecePosition(pos));
            });
        }
    }
}
