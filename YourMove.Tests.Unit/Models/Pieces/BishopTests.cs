﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using YourMove.Models.Moves;
using YourMove.Models.Pieces;
using YourMove.Models.Positions;

namespace YourMove.Tests.Unit.Models.Pieces
{
    public class BishopTests
    {
        [Fact]
        public void AllPossiblePositionsForMoveReturnsExpectedSetOfPositions()
        {
            var bishop = new Bishop(new PiecePosition("g5"));

            var allPossibleMoves = bishop.AllPossiblePositions();
            var expectedPositions = new List<string> {
                "h6", "f4", "e3", "d2", "c1", "h4", "f6",
                "e7", "d8"};

            Assert.Equal(expectedPositions.Count, allPossibleMoves.Count());

            Assert.All(expectedPositions, (string pos) => {
                Assert.Contains(allPossibleMoves, p => p == new PiecePosition(pos));
            });
        }

        [Fact]
        public void PossiblePositionsForMoveReturnsEmptyCollectionWhenMovementIsHorizontal()
        {
            var bishop = new Bishop(new PiecePosition("g5"));

            Assert.Empty(bishop.PossiblePositionsForMove(MoveTypes.Right));
        }
    }
}
