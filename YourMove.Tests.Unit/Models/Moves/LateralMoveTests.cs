﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using YourMove.Models.Moves;
using YourMove.Models.Positions;

namespace YourMove.Tests.Unit.Models.Moves
{
    public class LateralMoveTests
    {
        private LateralMove lateralMove;

        public LateralMoveTests()
        {
            lateralMove = new LateralMove();
        }

        [Fact]
        public void WhenApplicableMovesIsCalledOnlyLateralMoveTypesAreReturned()
        {
            var lateralMoves = new List<MoveTypes>() {
                MoveTypes.Up, MoveTypes.Right, MoveTypes.Down, MoveTypes.Left
            };
            var applicableLateralMoves = new HashSet<MoveTypes>(lateralMove.ApplicableMoves());

            Assert.True(applicableLateralMoves.Count == lateralMoves.Count,
                "'ApplicableMoves' does not have expected number of unique moves");
            Assert.True(lateralMoves.Except(applicableLateralMoves).Count() == 0,
                "'ApplicableMoves' does not have all expected moves");
        }

        [Fact]
        public void WhenCanHandleMoveIsGivenAllExpectedMovesAllResultsAreTrue()
        {

            Assert.True(lateralMove.CanHandleMove(MoveTypes.Up),
                "Expected applicable move flagged as not Applicable");
            Assert.True(lateralMove.CanHandleMove(MoveTypes.Right),
                "Expected applicable move flagged as not Applicable");
            Assert.True(lateralMove.CanHandleMove(MoveTypes.Down),
                "Expected applicable move flagged as not Applicable");
            Assert.True(lateralMove.CanHandleMove(MoveTypes.Left),
                "Expected applicable move flagged as not Applicable");
        }

        [Fact]
        public void WhenCanHandleMoveIsGivenUnexpectedMoveTypeFalseIsReturned()
        {
            Assert.False(lateralMove.CanHandleMove(MoveTypes.Diagonal10_30));
        }

        [Fact]
        public void WhenOneMoveIsCalledWithValidMovesWhenPositionIsWithAPositionNearMiddleExpectedNewPositionsAreReturned()
        {
            var d4 = new PiecePosition("d4");

            var e4 = new PiecePosition("e4");
            var c4 = new PiecePosition("c4");
            var d5 = new PiecePosition("d5");
            var d3 = new PiecePosition("d3");

            Assert.Equal(lateralMove.OneMove(d4, MoveTypes.Right), e4);
            Assert.Equal(lateralMove.OneMove(d4, MoveTypes.Up), d5);
            Assert.Equal(lateralMove.OneMove(d4, MoveTypes.Down), d3);
            Assert.Equal(lateralMove.OneMove(d4, MoveTypes.Left), c4);
        }

        [Fact]
        public void WhenPositionsCanStepGivenMoveWithEnoughRoomExpectedEndPositionsAreReturned()
        {
            var d4 = new PiecePosition("d4");
            var positionsCanMove = lateralMove.PositionsCanStep(d4, MoveTypes.Right, 2);

            Assert.Contains(new PiecePosition("e4"),
                positionsCanMove);
            Assert.Contains(new PiecePosition("f4"),
                positionsCanMove);
        }

        [Fact]
        public void WhenPositionsCanStepGivenNumberOfStepsGreaterThatExpectedLastPositionIsAtTheEdge()
        {
            var d4 = new PiecePosition("d4");
            var positionsCanMove = lateralMove.PositionsCanStep(d4, MoveTypes.Right, 12);

            Assert.True(positionsCanMove.Count() == 4);

            Assert.Contains(new PiecePosition("e4"),
                positionsCanMove);
            Assert.Contains(new PiecePosition("f4"),
                positionsCanMove);

            Assert.Contains(new PiecePosition("g4"),
                positionsCanMove);
            Assert.Contains(new PiecePosition("h4"),
                positionsCanMove);
        }

        [Fact]
        public void WhenPositionsCanStepGivenStepsFromEdgeToOffEdgeReturnsEmptySet()
        {
            var h4 = new PiecePosition("h4");
            var positionsCanMoveFromRightEdge = lateralMove.PositionsCanStep(h4, MoveTypes.Right, 12);

            var d1 = new PiecePosition("d1");
            var positionsCanMoveFromBottomEdge = lateralMove.PositionsCanStep(d1, MoveTypes.Down, 12);

            var d8 = new PiecePosition("d8");
            var positionsCanMoveFromTopEdge = lateralMove.PositionsCanStep(d8, MoveTypes.Up, 12);

            var a4 = new PiecePosition("a4");
            var positionsCanMoveFromLeftEdge = lateralMove.PositionsCanStep(a4, MoveTypes.Left, 12);

            Assert.True(positionsCanMoveFromRightEdge.Count() == 0, "Fall off right edge allowed");
            Assert.True(positionsCanMoveFromBottomEdge.Count() == 0, "Fall off bottom edge allowed");
            Assert.True(positionsCanMoveFromTopEdge.Count() == 0, "Fall off top edge allowed");
            Assert.True(positionsCanMoveFromLeftEdge.Count() == 0, "Fall off left edge allowed");
        }

        [Fact]
        public void WhenOneMoveIsCalledWithMoveThatWillBeOutOfRangeHNullIsReturned()
        {
            var h4 = new PiecePosition("H4");
            var d8 = new PiecePosition("d8");
            var d1 = new PiecePosition("d1");
            var a4 = new PiecePosition("a4");

            Assert.Null(lateralMove.OneMove(h4, MoveTypes.Right));
            Assert.Null(lateralMove.OneMove(d8, MoveTypes.Up));
            Assert.Null(lateralMove.OneMove(d1, MoveTypes.Down));
            Assert.Null(lateralMove.OneMove(a4, MoveTypes.Left));
        }
    }
}
