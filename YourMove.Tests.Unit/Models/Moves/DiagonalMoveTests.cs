﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using YourMove.Models.Moves;
using YourMove.Models.Positions;

namespace YourMove.Tests.Unit.Models.Moves
{
    public class DiagonalMoveTests
    {
        private DiagonalMove diagonalMove;

        public DiagonalMoveTests()
        {
            diagonalMove = new DiagonalMove();
        }

        [Fact]
        public void WhenApplicableMovesIsCalledOnlyLateralMoveTypesAreReturned()
        {
            var diagonalMoves = new List<MoveTypes>() {
                MoveTypes.Diagonal1_30, MoveTypes.Diagonal4_30,
                MoveTypes.Diagonal7_30, MoveTypes.Diagonal10_30
            };
            var applicableLateralMoves = new HashSet<MoveTypes>(diagonalMove.ApplicableMoves());

            Assert.True(applicableLateralMoves.Count == diagonalMoves.Count,
                "'ApplicableMoves' does not have expected number of unique moves");
            Assert.True(diagonalMoves.Except(applicableLateralMoves).Count() == 0,
                "'ApplicableMoves' does not have all expected moves");
        }

        [Fact]
        public void WhenCanHandleMoveIsGivenAllExpectedMovesAllResultsAreTrue()
        {

            Assert.True(diagonalMove.CanHandleMove(MoveTypes.Diagonal1_30),
                "Expected applicable move flagged as not Applicable");
            Assert.True(diagonalMove.CanHandleMove(MoveTypes.Diagonal4_30),
                "Expected applicable move flagged as not Applicable");
            Assert.True(diagonalMove.CanHandleMove(MoveTypes.Diagonal7_30),
                "Expected applicable move flagged as not Applicable");
            Assert.True(diagonalMove.CanHandleMove(MoveTypes.Diagonal10_30),
                "Expected applicable move flagged as not Applicable");
        }

        [Fact]
        public void WhenCanHandleMoveIsGivenUnexpectedMoveTypeFalseIsReturned()
        {
            Assert.False(diagonalMove.CanHandleMove(MoveTypes.Left));
        }

        [Fact]
        public void WhenOneMoveIsCalledWithValidMovesWhenPositionIsWithAPositionNearMiddleExpectedNewPositionsAreReturned()
        {
            var d4 = new PiecePosition("d4");

            var e5 = new PiecePosition("e5");
            var e3 = new PiecePosition("e3");
            var c3 = new PiecePosition("c3");
            var c5 = new PiecePosition("c5");

            Assert.Equal(diagonalMove.OneMove(d4, MoveTypes.Diagonal1_30), e5);
            Assert.Equal(diagonalMove.OneMove(d4, MoveTypes.Diagonal4_30), e3);
            Assert.Equal(diagonalMove.OneMove(d4, MoveTypes.Diagonal7_30), c3);
            Assert.Equal(diagonalMove.OneMove(d4, MoveTypes.Diagonal10_30), c5);
        }

        [Fact]
        public void WhenPositionsCanStepGivenMoveWithEnoughRoomExpectedEndPositionsAreReturned()
        {
            var d4 = new PiecePosition("d4");
            var positionsCanMove = diagonalMove.PositionsCanStep(d4, MoveTypes.Diagonal7_30, 2);

            Assert.Contains(new PiecePosition("c3"),
                positionsCanMove);
            Assert.Contains(new PiecePosition("b2"),
                positionsCanMove);
        }

        [Fact]
        public void WhenPositionsCanStepGivenNumberOfStepsGreaterThatExpectedLastPositionIsAtTheEdge()
        {
            var d4 = new PiecePosition("d4");
            var positionsCanMove = diagonalMove.PositionsCanStep(d4, MoveTypes.Diagonal10_30, 12);

            Assert.True(positionsCanMove.Count() == 3);

            Assert.Contains(new PiecePosition("c5"),
                positionsCanMove);

            Assert.Contains(new PiecePosition("b6"),
                positionsCanMove);

            Assert.Contains(new PiecePosition("a7"),
                positionsCanMove);
        }

        [Fact]
        public void WhenPositionsCanStepGivenStepsFromEdgeToOffEdgeReturnsEmptySet()
        {
            var h8 = new PiecePosition("h8");
            var positionsCanMoveFromTopRightEdge = diagonalMove.PositionsCanStep(h8, MoveTypes.Diagonal1_30, 12);

            var a1 = new PiecePosition("a1");
            var positionsCanMoveFromBottomLeftEdge = diagonalMove.PositionsCanStep(a1, MoveTypes.Diagonal7_30, 12);

            var a8 = new PiecePosition("a8");
            var positionsCanMoveFromTopLeftEdge = diagonalMove.PositionsCanStep(a8, MoveTypes.Diagonal10_30, 12);

            var h1 = new PiecePosition("h1");
            var positionsCanMoveFromBottomRighEdge = diagonalMove.PositionsCanStep(h1, MoveTypes.Diagonal4_30, 12);

            Assert.True(positionsCanMoveFromTopRightEdge.Count() == 0, "Fall off top-right edge allowed");
            Assert.True(positionsCanMoveFromBottomLeftEdge.Count() == 0, "Fall off bottom-left edge allowed");
            Assert.True(positionsCanMoveFromTopLeftEdge.Count() == 0, "Fall off top-left edge allowed");
            Assert.True(positionsCanMoveFromBottomRighEdge.Count() == 0, "Fall off bottom-right edge allowed");
        }

        [Fact]
        public void WhenOneMoveIsCalledWithMoveThatWillBeOutOfRangeHNullIsReturned()
        {
            var h8 = new PiecePosition("h8");
            var a1 = new PiecePosition("a1");
            var a8 = new PiecePosition("a8");
            var h1 = new PiecePosition("h1");

            Assert.Null(diagonalMove.OneMove(h8, MoveTypes.Diagonal1_30));
            Assert.Null(diagonalMove.OneMove(h1, MoveTypes.Diagonal4_30));
            Assert.Null(diagonalMove.OneMove(a1, MoveTypes.Diagonal7_30));
            Assert.Null(diagonalMove.OneMove(a8, MoveTypes.Diagonal10_30));
        }
    }
}
